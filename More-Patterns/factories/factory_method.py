from dataclasses import dataclass, field
from math import sin, cos

@dataclass
class Point:
    x: float = field(default=0.0)
    y: float = field(default=0.0)

    @staticmethod
    def new_cartesian_point(x: float, y: float):
        return Point(x, y)

    @staticmethod
    def new_polar_coordinate(rho: float, theta: float):
        return Point(rho * cos(theta), rho * sin(theta))

    def __str__(self):
        return f"{self.x, self.y}"

if __name__ == "__main__":
    p = Point().new_polar_coordinate(1, 2)
    print(p)
