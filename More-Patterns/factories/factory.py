from dataclasses import dataclass, field
from math import sin, cos

@dataclass
class Point:
    x: float = field(default=0.0)
    y: float = field(default=0.0)

    def __str__(self):
        return f"{self.x, self.y}"

    class PointFactory:
        def cartesian_point(self, x: float, y: float):
            p = Point()
            p.x, p.y = x, y
            return p

        def polar_coordinate(self, rho: float, theta: float):
            p = Point()
            p.x, p.y = rho * cos(theta), rho * sin(theta)
            return p

    new = PointFactory()

if __name__ == "__main__":
    p1 = Point().new.polar_coordinate(1, 2)
    print(p1)

    p2 = Point().new.cartesian_point(10, 11)
    print(p2)
