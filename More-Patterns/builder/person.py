from dataclasses import dataclass, field
from typing import Optional

@dataclass
class Person:
    # Address
    street_address : Optional[str] = None
    postcode : Optional[str] = None
    city : Optional[str] = None
    # Employment
    company_name : Optional[str] = None
    position : Optional[str] = None
    annual_income : Optional[float] = None

    def __str__(self) -> str:
        return f"""
            Address: {self.street_address}, {self.postcode}, {self.city},
            Employed at {self.company_name} as a {self.position} earning {str(self.annual_income)}.
        """

class PersonBuilder:
    def __init__(self, person=None):
        if person is None:
            self.person = Person()
        else:
            self.person = person

    @property
    def lives(self):
        return PersonAddressBuilder(self.person)

    @property
    def works(self):
        return PersonJobBuilder(self.person)

    def build(self):
        return self.person

class PersonJobBuilder(PersonBuilder):
    def __init__(self, person):
        super().__init__(person)

    def at(self, company_name: str):
        self.person.company_name = company_name
        return self

    def as_a(self, position: str):
        self.person.position = position
        return self

    def earning(self, annual_income: float):
        self.person.annual_income = annual_income
        return self

class PersonAddressBuilder(PersonBuilder):
    def __init__(self, person):
        super().__init__(person)

    def at(self, street_address: str):
        self.person.street_address = street_address
        return self

    def with_postcode(self, postcode: str):
        self.person.postcode = postcode
        return self

    def in_city(self, city: str):
        self.person.city = city
        return self

if __name__ == "__main__":
    pbuilder = PersonBuilder()
    p = (
        pbuilder
            .lives
                .at("1234 Somewhere")
                .in_city("Someplace")
                .with_postcode("0000-000")
            .works
                .at("Some Company")
                .as_a("Worker")
                .earning("Something")
            .build()
    )
    print(p)
