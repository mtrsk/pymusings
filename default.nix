{ pkgs ? import ./pinned-nixpkgs.nix {} }:

let
  sysPkgs = with pkgs; [
    python37Full
  ];
  pythonPkgs = with pkgs.python37Packages; [
    hypothesis
  ];
in
pkgs.stdenv.mkDerivation rec {
  name = "py-env-${version}";
  version = "0.0.1";
  buildInputs = sysPkgs ++ pythonPkgs;
}
