from __future__ import annotations
from threading import Lock, Thread
from typing import Optional

class HighlanderMeta(type):
    '''
    Thread-safe implementation of a Singleton

    A lock object is useful to synchronize threads
    when they first access our singleton
    '''
    _instance: Optional[Highlander] = None
    _lock : Lock = Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            '''
            The firs thread to adquire the lock
            reaches this point and creates the
            singleton instance.
            '''
            if not cls._instance:
                cls._instance = super.__call__(*args, **kwargs)
            return cls._instance

class Highlander(metaclass=HighlanderMeta):
    def __str__(self):
        return 'There can be only one!'

if __name__ == '__main__':
    a = Highlander()
    b = Highlander()

    print(f'ID(a) = {id(a)}')
    print(f'ID(b) = {id(b)}')
    print(f'Are they the same object? {a is b}')
