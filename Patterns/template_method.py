from abc import ABC, abstractmethod

class AverageCalculator(ABC):
    def average(self):
        try:
            num_itens = 0
            total_sum = 0
            while self.has_next():
                total_sum += self.next_item()
                num_itens += 1
            if num_itens == 0:
                raise RuntimeError('Can\'t compute the average of 0')
            return total_sum/num_itens
        finally:
            self.dispose()

    @abstractmethod
    def has_next(self):
        pass

    @abstractmethod
    def next_item(self):
        pass

    def dispose(self):
        pass


class FileAverageCalculator(AverageCalculator):
    def __init__(self, file_):
        self.file = file_
        self.last_line = self.file.readline()

    def has_next(self):
        return self.last_line != ''

    def next_item(self):
        result = float(self.last_line)
        self.last_line = self.file.readline()
        return result

    def dispose(self):
        self.file.close()


class MemoryAverageCalculator(AverageCalculator):
    def __init__(self, ls):
        self.ls = iter(ls)

    def has_next(self):
        return (self.ls.__length_hint__() > 0)

    def next_item(self):
        return next(self.ls)

if __name__ == '__main__':
    fd = open('data.txt')
    fac = FileAverageCalculator(fd)
    print(fac.average())

    mac = MemoryAverageCalculator([3])
    print(mac.average())

