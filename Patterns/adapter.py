from template_method import AverageCalculator, FileAverageCalculator
from random import randint

class GeneratorAdapter:
    def __init__(self, adaptee):
        self.adaptee = adaptee

    def readline(self):
        try:
            return next(self.adaptee)
        except StopIteration:
            return ''

    def close(self):
        pass

class Duck:
    def quack(self):
        print('Quack')

    def fly(self):
        print('I\'m Flying')

class Turkey:
    def gobble(self):
        print('Gobble Gobble')

    def fly(self):
        print('I\'m flying a short distance')

class TurkeyAdapter:
    def __init__(self, adaptee):
        self.adaptee = adaptee

    def quack(self):
        self.adaptee.gobble()

    def fly(self):
        for i in range(5):
            self.adaptee.fly()

def duck_interaction(duck):
    duck.quack()
    duck.fly()

if __name__ == '__main__':
    g = (randint(1,100) for i in range(1,10000))
    fac = FileAverageCalculator(GeneratorAdapter(g))
    print(fac.average())

    duck = Duck()
    turkey = Turkey()
    turkey_adapter = TurkeyAdapter(turkey)

    print('The Turkey says...')
    turkey.gobble()
    turkey.fly()

    print('\nThe Duck says...')
    duck_interaction(duck)

    print('\nThe TurkeyAdapter says...')
    duck_interaction(turkey_adapter)
